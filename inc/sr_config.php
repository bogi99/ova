<?php
	
	// define this elsewhere , this is just an example
	define ('CONFIG_FILE', "config.json")


	class sr_config
	{

		var $config = array();

		function __construct()
		{
			if(file_exists('CONFIG_FILE'))
			{
				// we read the config file and parse it into the config array
				$this->config = $this->read_config();
			}
			else
			{
				//just create an empty file for now
				file_put_contents(CONFIG_FILE,'');
			}

		}
	}

	// now we need a save config array to keep our file updated

	// also we need a read config and parse to array, to keep it fresh after opject init

	function read_config()
	{
		$this->config = json_decode(file_get_contents('CONFIG_FILE'), true);	
	}

	// we also need a delete config file, as an action to be called in case we need to do that
	function delete_config()
	{
		copy('CONFIG_FILE', 'CONFIG_FILE'.date("U"));
		
		unlink('CONFIG_FILE');
		clearstatcache(); 
		// i don't know if this needs some more work or not.
	}

?>
