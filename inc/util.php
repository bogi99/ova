<?php
    // Simple or not so simple dump function, handles variables, arrays and objects, classes 
	function dump($data, $type = 0)
	{
		if($type == 0)
		{
			echo "<pre>";
			print_r($data);
			echo "</pre>";
		}
		else
		{
			echo "<pre>";
			var_dump($data);
			echo "</pre>";
		}
	}
	
	// wrapper for dump, because shorthand is so good
	function dd($data, $type = 0)
	{
        return dump($data, $type );
	}
	
	// returnes true, and dump actually takes care of the class methods dump
	function dumpmethods($classname)
	{
		dump(get_class_methods( $classname ), 1);
	return true;
	}

	// generate a random string specified length from a list of given characters, currently english alphanumeric   
	function genrandomstring($length , $chars = ' 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
	{
		$string = '';
		for($i = 0; $i < $length; $i++ )
		{
			$string .= $chars[mt_rand(0, strlen($chars) - 1)];
		}
	return $string;
	}
	
	// uses the system wide LOG_ERRORS constant, make sure you define one, to start logging errors and potentially other messages 
	function logerrors()
	{
		if(LOG_ERRORS)
		{
			ini_set("log_errors", 1);
			ini_set("error_log", "php-errors.log");
			error_log( "Hello, errors!" );
		}
	}
	
	// generate a (large) number of (potentially small) files given a serialized name and a set length of random content (to defeat compression )
	function generate_small_files($number, $length)
	{
		for ($n=0; $n< $number ; $n++)
		{
			$string = genrandomstring($length);
			
			$nn = sprintf('%04d', $n);
			
			file_put_contents("testfile".$nn.".test",$string);
		}

	}
	
?>
