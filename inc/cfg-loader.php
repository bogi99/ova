<?php

	// config
	// ======
    // This is a config loader for production / development settings
    // check the code, it looks for a file named production in the settings folder
    // delete that file, and this reverts to development settings
    
	if(file_exists("settings/production"))
	{
		// production mode settings
		include_once("cfg/config.php");
	}
	else
	{
		// development settings
		include_once("cfg/config-dev.php");

	}

?>
