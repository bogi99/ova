<?php
include_once("inc/cfg-loader.php");
include_once("inc/util.php");

session_start();
// header("Cache-Control: no cache");
// session_cache_limiter("private_no_expire");
if($_SERVER['REQUEST_METHOD'] === 'POST')
{
	$_SESSION['postdata']= $_REQUEST;
	unset($_REQUEST);
	unset($_POST);
	header("Location: ".$_SERVER['PHP_SELF']." ");
}
?>
<!DOCTYPE html>

<html>
<head>
	<title>Ova</title>
	<meta HTTP-EQUIV="Pragma" content="no-cache">
	<meta HTTP-EQUIV="Expires" content="-1"> 
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="icon"       type="image/x-icon" href="img/favicon.ico"  />
	<link rel="stylesheet" type="text/css"     href="css/base.css">
	<link rel="stylesheet" type="text/css"     href="css/containers.css" />
	
</head>
<body>
<script>
function doDate()
{
    var str = "";

    var days = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
    var months = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

    var now = new Date();

    str += " " + days[now.getDay()] + ", " + now.getDate() + " " + months[now.getMonth()] + " " + now.getFullYear() + " " + now.getHours() +":" + now.getMinutes() + ":" + now.getSeconds();
    document.getElementById("todaysDate").innerHTML = str;
}

setInterval(doDate, 1000);
</script>
<?php
	if(!isset($authenticated) )
	{
		$authenticated = false;
	}
	if(isset($_SESSION['postdata']['action']))
	{
		if($_SESSION['postdata']['action'] == 'login')
		{
			$authenticated = true;
		}
		else if($_SESSION['postdata']['action'] == 'logout')
		{
			$authenticated = false;
		}
		
		
	}
	
?>
<div id="container" >
		
		<div id="top" >
			<div style=" display: grid; grid-column-gap: 10px; grid-row-gap: 10px; grid-template-columns: auto auto auto auto;">
				<div id="system"  style="grid-column-start: 1;" ><? echo SYSTEM_NAME." ".VERSION ;?></div>
				<div id="title" style="grid-column-start: 2;"><?php echo OVA_TITLE ;?></div>
				<div id="event" style="grid-column-start: 3;" >con</div>
				<div id="todaysDate" style="grid-column-start: 4;" ></div>
				<div id="login" style="grid-column-start: 1; grid-row-start:2" >
					<?php
					if($authenticated == false)
					{
					?>
					<form action="" method="post" #form1 >
					<input type="text"     name="uname" style="width: 60px; height:21px; line-height: 21px; padding: 1px 1px;" > 
					<input type="password" name="pass"  style="width: 60px; height:21px; line-height: 21px; padding: 1px 1px;" >
					<input type="hidden" name="action" value="login">
					<input type="submit"   name="login" value="Login" style="width: 80px; height:33px;" > 
					</form>
					<?php
					}
					if($authenticated == true)
					{ 
					?>
					<form action="" method="post" >
					<input type="submit"   name="logout" value="Logout" style="width: 80px; height:33px;" >
					<input type="hidden" name="action" value="logout">
					</form>
					<?php
					}
					?>
				</div>
				
			</div>
		</div>
		
		<div id="mid" >
			M
		</div>
		
		<div id="bottom">
			B
		</div>
		
	</div>
</body>
</html>
